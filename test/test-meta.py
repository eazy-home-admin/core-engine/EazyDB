# Example usage
from src.quick_yaml.data_structures import MetaData

metadata = MetaData()
metadata.add_table('user')
metadata.user.add_record('user_id', {'default': None, 'required': True, 'unique': True, 'type': str})
metadata.user.add_record('email', {'default': 'aabbbccc@gmail.com', 'required': True, 'unique': True, 'index': True, 'type': str})

# Accessing metadata like MongoDB collections
user_metadata = metadata.user
print(user_metadata.get_unique_columns())  # Output: ['user_id', 'email']


# Get a specific record
print(metadata.get_record('user', 'user_id'))  # Output: {'default': None, 'required': True, 'unique': True, 'type': str}

# Get metadata for a specific table
print(metadata.get_metadata('user'))  # Output: {'user_id': {'default': None, 'required': True, 'unique': True, 'type': <class 'str'>}, 'email': {'default': '', 'required': True, 'unique': True, 'index': True, 'type': <class 'str'>}}
print(f'user default values:{user_metadata.get_default_values()}')

# Modify and delete operations
metadata.update_record('user', 'user_id', {'unique': False})
user_metadata.delete_record('email')
metadata.delete_table('user')

# Example usage of from_dict
metadata_dict = {
    'metadata': {
        'products': {
            'product_id': {'default': None, 'required': True, 'unique': True, 'type': int},
            'name': {'default': '', 'required': True, 'unique': False, 'type': str},
            'price': {'default': 0.0, 'required': True, 'unique': False, 'type': float}
        },
        'orders': {
            'order_id': {'default': None, 'required': True, 'unique': True, 'type': int},
            'product_id': {'default': None, 'required': True, 'unique': False, 'type': int},
            'quantity': {'default': 1, 'required': True, 'unique': False, 'type': int}
        }
    }
}

metadata.from_dict(metadata_dict)
