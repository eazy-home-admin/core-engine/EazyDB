import unittest

from src.quick_yaml.data_structures import NestedDict


class TestNestedDict(unittest.TestCase):

    def test_set_and_get_item(self):
        nd = NestedDict()
        nd.set('a.b.c', 'value')
        self.assertEqual(nd.get('a.b.c'), 'value')

    def test_item_deletion(self):
        nd = NestedDict({'a': {'b': {'c': 'value'}}})
        del nd['a.b.c']
        self.assertFalse(nd.path_exists('a.b.c'))

    def test_len(self):
        nd = NestedDict({'a': {'b': 'value'}, 'b': 'value'})
        self.assertEqual(len(nd), 2)

    def test_merge(self):
        nd = NestedDict({'a': {'b': 'value1'}})
        nd_other = NestedDict({'a': {'c': 'value2'}, 'b': {'d': 'value3'}})
        nd.merge(nd_other)
        self.assertEqual(nd.get('a.c'), 'value2')
        self.assertEqual(nd.get('b.d'), 'value3')

    def test_update(self):
        nd = NestedDict({'a': {'b': 'value1'}})
        nd.update('a', {'b': 'new_value', 'c': 'value2'})
        self.assertEqual(nd.get('a.b'), 'new_value')
        self.assertEqual(nd.get('a.c'), 'value2')

    def test_path_exists(self):
        nd = NestedDict({'a': {'b': {'c': 'value'}}})
        self.assertTrue(nd.path_exists('a.b.c'))
        self.assertFalse(nd.path_exists('a.x.y'))

    def test_items_and_keys(self):
        nd = NestedDict({'a': {'b': 'value1'}, 'b': 'value2'})
        expected_keys = ['a.b', 'b']
        expected_items = [('a.b', 'value1'), ('b', 'value2')]
        self.assertEqual(sorted(nd.keys()), sorted(expected_keys))
        self.assertEqual(sorted(nd.items()), sorted(expected_items))

    def test_pretty_print(self):
        # Testing pretty_print is more about ensuring it doesn't raise errors
        # Since it's printing to stdout, we won't capture output here
        nd = NestedDict({'a': {'b': {'c': 'value'}}})
        try:
            nd.pretty_print()
            success = True
        except Exception:
            success = False
        self.assertTrue(success)


class TestNestedDictFindMethod(unittest.TestCase):
    def setUp(self):
        self.data = {
            'a': {
                'b': {
                    'c': 1,
                    'd': 2
                },
                'e': [3, 4, 5]
            },
            'f': 4
        }
        self.nd = NestedDict(self.data)

    def test_find_simple_path(self):
        result = self.nd.find('a.b.c')
        self.assertEqual(result, 1)

    def test_find_array_index(self):
        result = self.nd.find('a.e[0]')
        self.assertEqual(result, 3)

    def test_find_whole_array(self):
        result = self.nd.find('a.e')
        self.assertEqual(result, [3, 4, 5])

    def test_find_nonexistent_path(self):
        result = self.nd.find('a.x.y')
        self.assertIsNone(result)

    def test_find_with_wildcard(self):
        result = self.nd.find('a.b.*')
        self.assertIn(1, result)
        self.assertIn(2, result)

    # Add more complex tests as needed based on your JMESPath query requirements.



if __name__ == '__main__':
    unittest.main()
