# NestedDict

A class for managing nested dictionaries with dot notation access.

Attributes:
   -  _data (dict): The underlying data structure for the nested dictionary.

## Methods

### __delitem__

Deletes the item at the specified dot-separated keys.



**Parameters:**

- keys (str): The dot-separated keys of the item to delete.



**Raises:**

- KeyError: If any of the keys do not exist.

### __getitem__

Retrieve an item using a dot-separated key string.

### __init__

Initializes the NestedDict with the provided data or an empty dictionary.



**Parameters:**

- data (dict, optional): The initial dictionary data. Defaults to None, resulting in an empty dictionary.

### __iter__

Returns an iterator over the nested dictionary's keys and values.


**Returns:**

- An iterator that yields tuples of (dot-separated key string, value).

### __len__

Provides the length of NestedDict

### __setitem__

Retrieves an item using a dot-separated key string.



**Parameters:**

- keys (str): The dot-separated keys.



**Returns:**

- The value associated with the specified keys.



**Raises:**

- KeyError: If any of the keys do not exist.

### __str__

Returns a string representation of NestedDict that looks like a Python dictionary,
including handling nested NestedDict instances.

### _sort_recursive_by_key

Helper method to recursively sort the dictionary.

### _traverse

Pretty prints the nested dictionary to the console, with indentation to represent nesting.



**Parameters:**

- current (dict, optional): The current level of the dictionary to print. Used for recursion. Defaults to None, indicating the top level.
- indent (int, optional): The current indentation level. Defaults to 0.

### convert_nested

Recursively converts a dictionary and all of its nested dictionaries
into NestedDict instances for easier dot operator access.



**Parameters:**

- d (dict): The original dictionary to convert.



**Returns:**

- NestedDict: The converted dictionary where all nested dictionaries
- are also NestedDict instances.

### find

Use a JMESPath query string to find and return items from the nested dictionary.



**Parameters:**

- query (str): A JMESPath query string.



**Returns:**

- The result of the JMESPath query.

### flat_dict

Converts the nested dictionary into a flattened dictionary.



**Returns:**

- dict: A flattened dictionary representation of the nested dictionary.

### get

Retrieves an item, returning a default value if the specified keys do not exist.



**Parameters:**

- keys (str): The dot-separated keys.
- default (optional): The value to return if the keys do not exist. Defaults to None.



**Returns:**

- The value associated with the specified keys, or the default value if the keys do not exist.

### get_dict

_No method docstring._

### items

_No method docstring._

### keys

_No method docstring._

### merge

Merges another dictionary or NestedDict into this one, optionally overwriting existing values.



**Parameters:**

- other (NestedDict or dict): The other dictionary to merge into this one.
- overwrite (bool, optional): Whether to overwrite existing values. Defaults to True.

### nested_items

Returns a list of tuples (key, value) for items in the nested dictionary, optionally limited to a certain depth.



**Parameters:**

- level (int, optional): The maximum depth to traverse. Defaults to None, indicating no limit.



**Returns:**

- A list of tuples, where each tuple contains a dot-separated key string and its corresponding value.

### nested_keys

Returns a list of keys for the nested dictionary, optionally limited to a certain depth.



**Parameters:**

- level (int, optional): The maximum depth to traverse. Defaults to None, indicating no limit.



**Returns:**

- A list of dot-separated key strings.

### path_exists

Checks if a path exists in the NestedDict.



**Parameters:**

- keys (str): The dot-separated keys to check.



**Returns:**

- True if the path exists, False otherwise.

### pretty_print

Pretty print the nested dictionary.

### set

Sets the value at the specified dot-separated keys. Equivalent to using the item assignment.



**Parameters:**

- keys (str): The dot-separated keys where the value should be set.
- value: The value to set.
 

### sort_key

Sorts the dictionary by keys at the specified path or at the top level if no path is provided.



**Parameters:**

- path (str, optional): The dot-separated path to the target dictionary to sort. If not provided, the top-level dictionary is sorted.


**Returns:**

- A new `NestedDict` instance with the dictionary sorted at the specified path.

### to_dict

Recursively convert NestedDict into a standard dictionary.



**Returns:**

- dict: A dictionary representation of the NestedDict.

### to_pandas

Converts the nested dictionary into a pandas DataFrame.



**Returns:**

- pandas.DataFrame: A DataFrame representation of the nested dictionary.

### update

Updates data at a specified path with update_data.



**Parameters:**

- path (str): The dot-separated path to the target where the update should occur.
- update_data (dict): The data to update at the target location.



**Raises:**

- ValueError: If the target specified by the path is not a dictionary.

### values

Provides all the values.

**Returns:**
 list of dict values.

