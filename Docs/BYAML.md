    # BYAML

A class for handling the encoding and decoding of nested dictionaries into/from an encrypted or plain binary YAML format.

Attributes:
    - encryption_enabled (bool): Flag indicating whether encryption is enabled.
    - key_file (str): Path to the file containing the encryption key.
    - file_path (str): Path to the binary YAML file.
    - cipher_suite (Fernet): A Fernet instance initialized with the encryption key, used for encrypting and decrypting data.

## Methods

### __init__

Initializes the BYAML instance.



**Parameters:**

- filepath (str): The file path for the binary YAML data.
- encryption_enabled (bool, optional): Whether to enable encryption. Defaults to False.
- key_file (str, optional): The file path for the encryption key. Defaults to 'key.file'.

### convert_dict_to_byaml

Converts the dictionary to BYAML format, optionally encrypting


**Parameters:**

- data (dict): The dictionary data
- filename (str): The file path as string
- encrypted (bool): Whether to encrypt the binary YAML data. Defaults to False.


**Returns:**

- A string of path of the generated binary YAML file and the key file (if encrypted)
    

### convert_yaml_to_byaml

Converts a YAML file to binary YAML format, optionally encrypting it.



**Parameters:**

- filename (str): The path to the YAML file to convert.
- encrypted (bool, optional): Whether to encrypt the binary YAML data. Defaults to False.



**Returns:**

- A tuple containing the paths to the generated binary YAML file and the key file (if encryption is enabled).

### decode_from_binary_yaml

Reads the binary YAML data from the file path, optionally decrypting it, and decodes it into a NestedDict.


**Parameters:**

- type_dict: The type of dictionary either 'dictionary' or 'NestedDict'. If not specified, it defaults to 'NestedDict'.


**Returns:**

- A NestedDict/Dict instance containing the decoded data.

### encode_to_binary_yaml

Encodes the given data into binary YAML format, optionally encrypting it, and writes it to the file path.



**Parameters:**

- data (NestedDict or dict): The data to encode and save.



**Raises:**

- Exception: If an error occurs during the encoding or file writing process.

### generate_and_save_key

Generates a new encryption key and saves it to the key file.



**Returns:**

- The generated encryption key.

### load_key

Loads the encryption key from the key file.



**Returns:**

- The encryption key. None if the encryption key cannot be loaded.

