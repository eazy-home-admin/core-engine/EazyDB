from src.quick_yaml.data_structures.locker import RecordLockManager
from src.quick_yaml.manager import QuickYaml
from src.quick_yaml.parser import QueryProcessor


class QuickYamlFine(QuickYaml):
    """
    A subclass of QuickYaml that implements fine-grained locking. It implements read/write lock in record level.

    """

    def __init__(self, path, key_file='key.file',encrypted=False, auto_load=True, **kwargs):

        super().__init__(path, key_file,encrypted,auto_load, **kwargs)
        self._lock_manager = RecordLockManager(create_logger=self.logger_enabled, logger=self._logger)

    def insert_new_data(self, table_name, data):
        """
        Insert new data into the table in a thread-safe manner.

        Parameters:
            table_name (str): Name of the table.
            data (dict): Data to insert.

        Returns:
            str: A message indicating the insertion operation is done.

        Raises:
            ValueError: If the table does not exist, the entry does not exist, or a unique constraint is violated.
        """
        if table_name not in self.tables:
            raise ValueError("Table does not exist.")

        # Generate a unique Record ID for the new data
        entry_id = self._generate_new_id(table_name)

        # Acquire a write lock for the newly generated ID
        self._lock_manager.acquire_writer_lock(table_name, entry_id)

        try:
            # Check for unique constraints
            unique_columns = self.tables[table_name]['metadata'].get_unique_columns(table_name)
            if any(data.get(column) in [row.get(column) for row in self.tables[table_name]['data'].values()] for column
                   in unique_columns):
                raise ValueError("Unique constraint violated.")

            # Insert the data
            self.tables[table_name]['data'][entry_id] = data
            self.save_db()
            self._logger.debug(f"Inserted data for record {entry_id} into table {table_name}")
        except Exception as e:
            raise e
        finally:
            # Release the write lock
            self._lock_manager.release_writer_lock(table_name, entry_id)

        return "done."

    def update_entry(self, table_name, entry_id, updated_data):
        """"""
        if table_name not in self.tables or entry_id not in self.tables[table_name]['data'].keys():
            raise ValueError("Table or entry does not exist.")
        try:
            self._lock_manager.acquire_writer_lock(table_name, entry_id)
            self.tables[table_name]['data'][entry_id].update(updated_data)
            self.save_db()

        except Exception as e:
            raise e
        finally:
            # Release the lock
            self._lock_manager.release_writer_lock(table_name, entry_id)

        return "done."

    def update_many(self, table_name, condition, update_data, flags=None):
        """
        Updates data based on given condition.
        Parameters:
            condition (dict): Filtering Conditions.
            update_data (dict): Data to be updated.
            flags: Additional flags (Supported: { add_missing_values : 'True'/False}).
            table_name: Name of table.
        """
        if flags is None:
            flags = {'add_missing_values': True}
        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")
        self._log('Updating table.........')
        # Retrieve the metadata to check for unique constraints
        self._lock_for_read(table_name)
        qp = QueryProcessor(self.tables[table_name]['data'])
        qp.filter(condition,index_only=True)
        matching_ids = qp.results
        self._release_read(table_name)
        if not matching_ids:
            return None  # No data matching the condition

        # Sort IDs to avoid deadlock
        matching_ids.sort()

        locked_ids = []
        try:
            # Acquire locks for all relevant records
            # print('Acquiring lock for ids.........')
            for entry_id in matching_ids:
                self._lock_manager.acquire_writer_lock(table_name, entry_id)
                locked_ids.append(entry_id)
            # Perform updates
            super().update_many(table_name, condition, update_data, flags)
        except Exception as e:
            raise e
        finally:
            # Release locks
            for entry_id in locked_ids:
                self._lock_manager.release_writer_lock(table_name, entry_id)

        return "done"

    def delete_entry(self, table_name, entry_id):
        """"""
        if table_name not in self.tables or entry_id not in self.tables[table_name]['data'].keys():
            raise ValueError("Table or entry does not exist.")
        try:
            self._lock_manager.acquire_writer_lock(table_name, entry_id)
            del self.tables[table_name]['data'][entry_id]

            self.save_db()
        except Exception as e:
            raise e
        finally:
            # Release the lock
            self._lock_manager.release_writer_lock(table_name, entry_id)
            self._lock_manager.delete_lock_id(table_name, entry_id)

        return "done"

    def delete_table(self, table_name):
        """"""

        if table_name not in self.tables:
            raise ValueError("Table does not exist.")

        # Acquire lock for entire id
        for i in self.tables[table_name]['data'].keys():
            self._lock_manager.acquire_writer_lock(table_name, i)
        del self.tables[table_name]

        self.save_db()

        self._lock_manager.delete_lock_table(table_name)

        return "done"

    def delete_many(self, table_name, condition):
        """
        Delete multiple records from a table based on a given condition.
        Threadsafe
        Parameters:
            table_name (str): The name of the table to delete records from.
            condition (dict): The condition to filter the records to be deleted.
        Raises:
            ValueError: If the table does not exist in the database.
        Returns:
            str: A message confirming the deletion process is done.
        """
        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")

        # Retrieve the metadata to check for unique constraints
        qp = QueryProcessor(self.tables[table_name]['data'])
        qp.filter(condition,index_only=True)
        matching_ids = qp.results

        if not matching_ids:
            return None  # No data matching the condition

        # Sort IDs to avoid deadlock
        matching_ids.sort()

        locked_ids = []
        try:
            # Acquire locks for all relevant records
            for entry_id in matching_ids:
                self._lock_manager.acquire_writer_lock(table_name, entry_id)
                locked_ids.append(entry_id)

            # Perform updates
            for entry_id in matching_ids:
                del self.tables[table_name]['data'][entry_id]

            self.save_db()

        except Exception as e:
            raise e
        finally:
            # Release locks
            for entry_id in locked_ids:
                self._lock_manager.release_writer_lock(table_name, entry_id)
                # remove all the locks associated with id
                self._lock_manager.delete_lock_id(table_name, entry_id)

        return "done"

    def get_all_data(self, table_name):
        """
        Returns all the record.
        Returns:
            list: A list of all the records in the table.
        Raises:
            ValueError: If the table does not exist in the database.
        """
        if table_name not in self.tables:
            raise ValueError("Table does not exist.")

        self._lock_for_read(table_name)
        results = self.tables[table_name]['data'].values()
        self._release_read(table_name)
        return results

    def find(self, table_name, query):
        """
        Find for specific records based on $filter query

        Parameters:
            table_name (str): The name of the table to search in.
            query (dict): The query to filter the data.

        Returns:
            list: The results of the query execution.
        Raises:
            ValueError: If the table does not exist in the database.
        """
        if table_name not in self.tables:
            raise ValueError("Table does not exist.")

        self._lock_for_read(table_name)
        qp = QueryProcessor(self.tables[table_name])
        results = qp.filter(query)
        self._release_read(table_name)
        return results

    def get_data_by_id(self, table_name, entry_id):
        """Gets Data by Object ID"""

        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")

        if isinstance(entry_id, int):
            # convert it to string
            entry_id = str(entry_id)
        try:
            self._lock_manager.acquire_reader_lock(table_name, entry_id)

            data = self.tables[table_name]['data'][entry_id]

        except Exception as e:
            raise e
        finally:
            # Release the lock
            self._lock_manager.release_reader_lock(table_name, entry_id)
        return data

    def execute_query(self, table_name, query):
        """Executes the query on the table.
        Parameters:
            table_name (str): The name of the table to execute the query on.
            query (dict): The query to be executed.
        """

        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")

        self._lock_for_read(table_name)
        qp = QueryProcessor(self.tables[table_name])
        results = qp.process_query(query)
        self._release_read(table_name)
        return results

    def _lock_for_read(self, table_name):
        """
        Lock All the record in the table for reading

       **DO NOT USE THIS FUNCTION. IT IS MEANT FOR INTERNAL PURPOSES.**

        Parameters:
            table_name (str): The name of the table to lock.

        Returns:
            None
        """
        self._log('Waiting to acquire reader lock for entire table.', 'debug')

        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")

        k = self.tables[table_name]['data'].keys()
        # lock for all data
        for i in k:
            self._lock_manager.acquire_reader_lock(table_name, i)
        pass

    def _release_read(self, table_name):
        """
        Release All the record in the table for reading

       **DO NOT USE THIS FUNCTION.
       IT IS MEANT FOR INTERNAL PURPOSES.**

        Parameters:
            table_name (str): The name of the table to lock.

        Returns:
            None
        """

        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")
        self._log('Releasing reader lock for entire table.', 'debug')
        # lock for all data
        for i in self.tables[table_name]['data'].keys():
            self._lock_manager.release_reader_lock(table_name, i)

        self._log(f"Released reader lock for the table. {table_name}", 'debug')

    # _lock_write

    def _lock_for_write(self, table_name):
        """
        Lock All the record in the table for writing

       **DO NOT USE THIS FUNCTION. IT IS MEANT FOR INTERNAL PURPOSES.**

        Parameters:
            table_name (str): The name of the table to lock.

        Returns:
            None
        """
        self._log('Waiting to acquire writer lock for entire table.', 'debug')
        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")
        key_list = list(self.tables[table_name]['data'].keys())

        self._log('Waiting to acquire writer lock for entire table.', 'debug')
        key_list.sort()
        # lock for all data
        for i in key_list:
            self._lock_manager.acquire_writer_lock(table_name, i)

        self._log(f'Acquired writer lock for the table. {table_name}', 'debug')

    def _release_write(self, table_name):
        """
        Release All the record in the table for writing

       **DO NOT USE THIS FUNCTION. IT IS MEANT FOR INTERNAL PURPOSES.**

        Parameters:
            table_name (str): The name of the table to lock.

        Returns:
            None
        """
        print('Releasing write lock for all table')
        if table_name not in self.tables:
            raise ValueError(f"Table '{table_name}' does not exist.")

        self._log('Releasing writer lock for entire table.', 'debug')
        # lock for all data
        for i in self.tables[table_name]['data'].keys():
            self._lock_manager.release_writer_lock(table_name, i)

        self._log(f"Released writer lock for the table. {table_name}", 'debug')