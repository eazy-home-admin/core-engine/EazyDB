__all__ = ['QuickYaml', 'QuickYamlCoarse', 'QuickYamlFine']

from datetime import time

# Create Alias for for compatibility with previous version

from src.quick_yaml.data_structures.locker import RecordLock, RecordLockManager
from threading import Semaphore, Lock

from src.quick_yaml.manager import QuickYaml
from src.quick_yaml.parser import QueryProcessor


class QuickYamlCoarse(QuickYaml):
    """
     A subclass designed to handle locks in a coarse-grained manner. This is ideal for environments
     where simplicity and preventing race conditions are prioritized over operation throughput. Inherits from QuickYaml.

    Attributes:
        _mutex (threading.Semaphore): A semaphore used to synchronize access to the database.
        _writers_lock (threading.Semaphore): A semaphore used to synchronize access t  Inherits from:
        _readers (int): The number of _readers currently accessing the database.
    """

    READ_OPERATIONS = ["find", "get_all_data", "execute_query", "get_data_by_id", "to_pandas",'get_all_data','make_backup']
    WRITE_OPERATIONS = ["insert_many", "insert_new_data", "update_entry", "update_many", "delete_entry",
                        "delete_many", "create_table", "update_meta_data", "insert_meta_data",'restore_backup']

    # include all the arguments from base class
    def __init__(self, path, key_file='key.file', encrypted=False, auto_load=True, **kwargs):
        super().__init__(path, key_file,encrypted, auto_load, **kwargs)

        self._mutex = Semaphore(1)
        self._writers_lock = Semaphore(1)
        self._readers = 0
        self._writers_waiting = False
        self._data_lock = Lock()

    def _start_write(self, agent='Writer'):
        """
        Function to acquire the writer lock.
        """
        self._log(f'{agent} Waiting to acquire writer lock. With {self._readers} _readers', 'debug')
        with self._mutex:
            self._writers_waiting = True

        self._log(f'{agent} Acquired writer lock. With {self._readers} _readers', 'debug')
        self._writers_lock.acquire()

    def _end_write(self, agent='Writer'):
        """
        Function to release the writer lock.
        """
        self._log(f'Releasing writer lock. With {self._readers} _readers', 'debug')
        self._writers_lock.release()

        with self._mutex:
            self._writers_waiting = False

        self._log(f'Released writer lock.With {self._readers} _readers', 'debug')

    def _start_read(self):
        """
        Function to acquire the reader lock.
        """
        self._log('Waiting to acquire reader lock', 'debug')
        self._mutex.acquire()  # Acquire mutex lock to synchronize readers attaining lock
        while self._writers_waiting:
            # Release all the readers until writers are finished.
            self._log(f'Lock Released due to waiting of writers. No of _readers {self._readers}')
            self._mutex.release()  # Release mutex in favor of writers
            time.sleep(0.1)  # Sleep to prevent busy waiting
            self._mutex.acquire()  # Again Try to acquire locks.

        self._log('Acquired reader lock', 'debug')
        self._readers += 1

        if self._readers == 1:
            self._log('Waiting for writers lock by _readers', 'debug')
            self._writers_lock.acquire()
            self._log('Writers Lock acquired by _readers.', 'debug')
        self._mutex.release()
        self._log(f'No of _readers {self._readers}', 'debug')

    def _end_read(self):
        """Function to release reader lock."""
        self._log('Releasing reader lock', 'debug')
        self._mutex.acquire()

        self._readers -= 1
        self._log(f'Released reader lock. No of Readers {self._readers}', 'debug')
        if self._readers == 0:
            self._writers_lock.release()
        self._mutex.release()

    def __getattr__(self, item):
        """Override the getattr method to add read/write locks to the database operations."""
        f = getattr(super(), item)

        if item in QuickYamlCoarse.WRITE_OPERATIONS:
            def wrapped(*args, **kwargs):
                # modify the function to add locks
                try:
                    self._start_write()
                    return f(*args, **kwargs)
                finally:
                    self._end_write()

            return wrapped

        elif item in QuickYamlCoarse.READ_OPERATIONS:
            def wrapped(*args, **kwargs):
                try:
                    self._start_read()
                    return f(*args, **kwargs)
                finally:
                    self._end_read()

            return wrapped
        else:
            return f



# Variables for previous release compatibility

