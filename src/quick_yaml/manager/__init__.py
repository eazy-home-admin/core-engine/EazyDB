from src.quick_yaml.manager.qyaml import QuickYaml
from src.quick_yaml.manager.qyaml_coarse import QuickYamlCoarse
from src.quick_yaml.manager.qyaml_fine import QuickYamlFine
__all__ = ['QuickYaml', 'QuickYamlCoarse', 'QuickYamlFine']

QYAMLDB = QuickYaml

QYAMLDBFine = QuickYamlFine
QYAMLDBCoarse = QuickYamlCoarse
