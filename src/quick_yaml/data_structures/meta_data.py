import copy
from threading import Lock
import pprint
import re

import numpy as np
from numpy.array_api import unique_counts



class TableMetaData:
    """
    A class for storing metadata for a specific table.
    Attributes:
        - table_name (str): The name of the table.
        - records (dict): A dictionary of records for the table.
    """

    def __init__(self, table_name):
        self.table_name = table_name
        self.records = {}  # Each record will be like this {'column': {'default': 'value', 'required': bool, 'unique': bool, 'type': type}}
        # these variables are caching purpose
        self._unique_columns = {}
        self._column_types = {}
        self._required_columns = {}
        self._default_values = {}
        self._not_null_columns = {}
        self._not_empty_columns = {}
        self._ranges = {}  # int/float only
        self._patterns = {}  # str only
        self._allowed_values = {}  # anything

    def to_dict(self):
        """Return a dictionary representation of the table metadata."""
        return self.records

    def add_record(self, column, record_details):
        """
        Parameters:
            column (str): The name of the column to add the record to.
            record_details (dict): The details of the record to add.
        Returns:
            None
        """
        # Print all args column, record_details
        # check formatting
        # if nothing is there populate with default values
        if 'default' not in record_details:
            record_details['default'] = None
        if 'required' not in record_details:
            record_details['required'] = False
        if 'unique' not in record_details:
            record_details['unique'] = False
        if 'type' not in record_details:
            record_details['type'] = type
        if 'not_empty' not in record_details:
            record_details['not_empty'] = False
        if 'not_null' not in record_details:
            record_details['not_null'] = False

        # We need to support range for int and float
        if record_details['type'] == int or record_details['type'] == float:
            if 'range' not in record_details:
                record_details['range'] = {'min': None, 'max': None}

        # For strings, length, pattern accepted/contains, allowed values
        if record_details['type'] == str:
            if 'length' not in record_details:
                record_details['length'] = None
            if 'pattern' not in record_details:
                record_details['pattern'] = re.compile(record_details.get('pattern', '.*'))
            if 'allowed_values' not in record_details:
                record_details['allowed_values'] = None  # Full match requirement for specific allowed values

        self.records[column] = record_details
        self._update_cache_on_add(column, record_details)

    def _update_cache_on_add(self, column, record_details):
        """Update cache dictionaries when adding a record."""
        if record_details.get('unique'):
            self._unique_columns[column] = True
        if record_details.get('required'):
            self._required_columns[column] = True
        if record_details.get('default') is not None:
            self._default_values[column] = record_details['default']
        if record_details.get('type') is not None:
            self._column_types[column] = record_details['type']
        if record_details.get('not_null'):
            self._not_null_columns[column] = True
        if record_details.get('not_empty'):
            self._not_empty_columns[column] = True
        if 'range' in record_details and record_details['type'] in [int, float]:
            self._ranges[column] = record_details['range']
        if 'pattern' in record_details and record_details['type'] == str:
            self._patterns[column] = record_details['pattern']
        if 'allowed_values' in record_details:
            self._allowed_values[column] = record_details['allowed_values']

    def delete_record(self, column):
        """Delete the record details for a specific column.
        Parameters:
            column (str): The name of the column to delete the record from.
        Returns:
            None
        """
        if column in self.records:
            del self.records[column]
            self._update_cache_on_delete(column)

    def _update_cache_on_delete(self, column):
        """Update cache dictionaries when deleting a record."""
        self._unique_columns.pop(column, None)
        self._required_columns.pop(column, None)
        self._default_values.pop(column, None)
        self._column_types.pop(column, None)
        self._not_null_columns.pop(column, None)
        self._not_empty_columns.pop(column, None)
        self._ranges.pop(column, None)
        self._patterns.pop(column, None)
        self._allowed_values.pop(column, None)

    def modify_record(self, column, record_details):
        """
        Parameters:
            column (str): The name of the column to modify the record in.
            record_details (dict): The details of the record to modify.
        Returns:
            None
        """
        if column in self.records:
            self.records[column].update(record_details)  # update record_details
            self._update_cache_on_modify(column, record_details)

    def _update_cache_on_modify(self, column, record_details):
        """Update cache dictionaries when modifying a record."""
        if 'unique' in record_details:
            if record_details['unique']:
                self._unique_columns[column] = True
            else:
                self._unique_columns.pop(column, None)
        if 'required' in record_details:
            if record_details['required']:
                self._required_columns[column] = True
            else:
                self._required_columns.pop(column, None)
        if 'default' in record_details:
            if record_details['default'] is not None:
                self._default_values[column] = record_details['default']
            else:
                self._default_values.pop(column, None)
        if 'type' in record_details:
            if record_details['type'] is not None:
                self._column_types[column] = record_details['type']
            else:
                self._column_types.pop(column, None)
        if 'not_null' in record_details:
            if record_details['not_null']:
                self._not_null_columns[column] = True
            else:
                self._not_null_columns.pop(column, None)
        if 'not_empty' in record_details:
            if record_details['not_empty']:
                self._not_empty_columns[column] = True
            else:
                self._not_empty_columns.pop(column, None)
        if 'range' in record_details and record_details['type'] in [int, float]:
            self._ranges[column] = record_details['range']
        else:
            self._ranges.pop(column, None)
        if 'pattern' in record_details and record_details['type'] == str:
            self._patterns[column] = record_details['pattern']
        else:
            self._patterns.pop(column, None)
        if 'allowed_values' in record_details:
            self._allowed_values[column] = record_details['allowed_values']
        else:
            self._allowed_values.pop(column, None)

    # Method to validate them aall!!!
    def add_missing_defaults(self,data):
        """
        Adds missing default values to the data dictionary.
        Parameters:
            data (dict): The data dictionary to add missing defaults to.
        returns:
             data(dict): The updated data dictionary with missing defaults added.
        """
        for column in self._default_values:
            if column not in data:
                data[column] = self._default_values[column]
        return data

    def validate_not_null(self, data):
        """
        Validates that all required columns are present in the data dictionary.
        Parameters:
            data (dict): The data dictionary to validate.
        Returns:
            bool: True if all required columns are present, False otherwise.
        """
        for column,value in self._not_null_columns.items():
            if value:
                if column not in data or data[column] is None or np.isnan(data[column]):
                    return False, column
        return True

    def validate_not_empty(self, data):
        """
        Validates that all required columns are present in the data dictionary.
        Parameters:
            data (dict): The data dictionary to validate.
        Returns:
            bool: True if all required columns are present, False otherwise.
        """
        # For this, We must check if values are None/NaN as well
        ret = self.validate_not_null(data)
        if not ret[0]:
            return ret
        for column,value in self._not_empty_columns.items():
            if value:
                if column not in data or data[column] == "":
                    return False, column
        return True

    def validate_unique(self, data,actual_table_data):
        """
        Validates that all unique columns are unique in the data dictionary.
        Parameters:
            data (dict): The data dictionary to validate.
            actual_table_data (dict): The actual data in the table.
        Returns:
            bool: True if all unique columns are unique, False otherwise.

        """
        for column in self._unique_columns:
            if column in data and any(
                    data[column] == row.get(column) for row in actual_table_data.values()):
                raise ValueError(f"Unique constraint violated for column: {column}")

        return True

    def validate_in_range(self, data):
        """
        Validates that all range columns are within the specified range.
        Parameters:
            data (dict): The data dictionary to validate.
        Returns:
            bool: True if all range columns are within the range, False otherwise.
        """
        for column in self._ranges:
            if column in data and data[column] < self._ranges[column][0] or data[column] > self._ranges[column][1]:
                return False, column
        return True

    def get_record(self, column):
        """Get the record details for a specific column."""
        return self.records.get(column, None)

    def get_unique_columns(self):
        """
        Return a dictionary of unique columns in the table.
        """
        return self._unique_columns

    def get_required_column(self):
        """
        Return a dictionary of required columns in the table.
        """
        return self._required_columns

    def get_default_values(self):
        """
        Return a dictionary of default values for columns in the table.
        """
        return self._default_values

    def get_column_type(self):
        """
        Return a dictionary of column types in the table.
        """
        return self._column_types

    def get_column_not_null(self):
        """
        Return a dictionary of columns that cannot be null in the table.
        """
        return self._not_null_columns

    def get_column_not_empty(self):
        """
        Return a dictionary of columns that cannot be empty in the table.
        """
        return self._not_empty_columns

    def get_column_allowed_values(self):
        return self._allowed_values

    def get_column_pattern(self):
        return self._patterns

    def get_column_range(self):
        return self._ranges



class MetaData:
    """
    A class for storing table metadata
    """
    def __init__(self):
        self.tables = {}  # A dictionary to hold table-specific metadata
        self._lock = Lock()  # Internal Semaphore for synchronizing access to the metadata.

    def from_dict(self, metadata_dict):
        """
        Load metadata from a dictionary.
        Parameters:
            metadata_dict (dict): A dictionary containing metadata for multiple tables.
        Returns:
            None
        """
        with self._lock:
            for table_name, table_metadata in metadata_dict.get('metadata', {}).items():
                self.tables[table_name] = TableMetaData(table_name)
                for column, column_metadata in table_metadata.items():
                    self.tables[table_name].add_record(column, column_metadata)

    def add_table(self, table_name):
        """
        Add a new table to the metadata.
        Parameters:
            table_name (str): The name of the table.
        Returns:
            None
        """
        with self._lock:
            table_metadata = TableMetaData(table_name)
            self.tables[table_name] = table_metadata

    def delete_table(self, table_name):
        """
        Delete a table from the metadata.
        Parameters:
            table_name (str): The name of the table to delete.
        Returns:
            None
        """
        with self._lock:
            if table_name in self.tables:
                del self.tables[table_name]

    def add_record(self, table_name, column, record_details):
        """
        Add a new record to the specified table.
        Parameters:
            table_name (str): The name of the table to add the record to.
            column (str): The name of the column to add the record to.
            record_details (dict): The details of the record to add.
        Returns:
            None
        """
        with self._lock:
            if table_name in self.tables:
                self.tables[table_name].add_record(column, record_details)

    def update_record(self, table_name, column, record_details):
        """
        Update a record in the specified table.
        Parameters:
            table_name (str): The name of the table to update the record in.
            column (str): The name of the column to update the record in.
            record_details (dict): The details of the record to update.
        Returns:
            None
        """
        with self._lock:
            if table_name in self.tables:
                self.tables[table_name].modify_record(column, record_details)

    def get_record(self, table_name, column):
        """
        Get a record from the specified table.
        Parameters:
            table_name (str): The name of the table to get the record from.
            column (str): The name of the column to get the record from.
        Returns:
            dict: The record details if found, None otherwise.
        """
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_record(column)
            return None

    def get_metadata(self, table_name):
        """
        Get the metadata for a specific table.
        Parameters:
            table_name (str): The name of the table to get the metadata for.
        Returns:
            dict: The metadata of the specified table if found, None otherwise.
        """
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].to_dict()
            return None

    def get_unique_columns(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_unique_columns()
            return None

    def get_required_columns(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_required_column()
            return None

    def get_default_values(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_default_values()
            return None

    def get_column_types(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_column_type()
            return None

    def get_column_not_null(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_column_not_null()
            return None

    def get_column_not_empty(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_column_not_empty()
            return None

    def get_column_allowed_values(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_column_allowed_values()
            return None

    def get_column_patterns(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_column_pattern()
            return None

    def get_column_ranges(self, table_name):
        with self._lock:
            if table_name in self.tables:
                return self.tables[table_name].get_column_range()
            return None

    def to_dict(self):
        """
        Return a dictionary representation of the object.
        """
        with self._lock:
            return {
                'metadata': {
                    table_name: table.to_dict()
                    for table_name, table in self.tables.items()
                }
            }

    def __getattr__(self, item):
        """
        Allow dot notation access to table metadata, similar to MongoDB collections.
        """
        with self._lock:
            if item in self.tables:
                return self.tables[item]
            raise AttributeError(f"'{type(self).__name__}' object has no attribute '{item}'")

    def __deepcopy__(self, memo):
        # Create a new instance of the same class
        new_obj = type(self)()
        # Explicitly copy the dictionary to the new instance
        new_obj.tables = copy.deepcopy(self.tables, memo)
        # Do not copy the lock,
