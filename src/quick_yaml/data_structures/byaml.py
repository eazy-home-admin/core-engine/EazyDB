import base64
import os
import shutil
import time
import yaml
from src.quick_yaml.data_structures.nested_dict import NestedDict
from cryptography.fernet import Fernet

class BYAML:
    """
      A class for handling the encoding and decoding of nested dictionaries into/from an encrypted or plain binary YAML format.

      Attributes:
          encryption_enabled (bool): Flag indicating whether encryption is enabled.
          key_file (str): Path to the file containing the encryption key.
          file_path (str): Path to the binary YAML file.
          cipher_suite (Fernet): A Fernet instance initialized with the encryption key, used for encrypting and decrypting data.
      """

    def __init__(self, filepath, encryption_enabled=False, key_file='key.file',
                 **kwargs):
        """
          Initializes the BYAML instance.

          Parameters:
              filepath (str): The file path for the binary YAML data.
              encryption_enabled (bool, optional): Whether to enable encryption. Defaults to False.
              key_file (str, optional): The file path for the encryption key. Defaults to 'key.file'.
        """

        self.encryption_enabled = encryption_enabled
        self.key_file = key_file
        self.file_path = filepath
        self._backup_path = kwargs.get('backup_path', os.path.join('..', 'backups'))
        if encryption_enabled:
            self.key = self.load_key() or self.generate_and_save_key()
            self.cipher_suite = Fernet(self.key)

    @property
    def backup_path(self):
        return self._backup_path

    @backup_path.setter
    def backup_path(self, path):
        self._backup_path = path

    def generate_and_save_key(self):
        """
          Generates a new encryption key and saves it to the key file.

          Returns:
              The generated encryption key.
        """
        key = Fernet.generate_key()
        with open(self.key_file, 'wb') as file:
            file.write(key)
        return key

    def load_key(self):
        """
           Loads the encryption key from the key file.

           Returns:
               The encryption key. None if the encryption key cannot be loaded.
        """
        try:
            with open(self.key_file, 'rb') as file:
                return file.read()
        except FileNotFoundError:
            return None  # Returning None to allow for key generation if not found

    def encode_to_binary_yaml(self, data):
        """
           Encodes the given data into binary YAML format, optionally encrypting it, and writes it to the file path.

           Parameters:
               data (NestedDict or dict): The data to encode and save.

           Raises:
               Exception: If an error occurs during the encoding or file writing process.
        """
        try:
            if type(data) is NestedDict:
                yaml_str = yaml.dump(data.to_dict())
            else:
                yaml_str = yaml.dump(data)
            binary_yaml = base64.b64encode(yaml_str.encode('utf-8'))
            if self.encryption_enabled:
                binary_yaml = self.cipher_suite.encrypt(binary_yaml)
            with open(self.file_path, 'wb+') as file:
                file.write(binary_yaml)
        except Exception as e:
            raise Exception('Exception occurred during encode to binary yaml operation.') from e

    def decode_from_binary_yaml(self, type_dict: str = 'NestedDict'):
        """
           Reads the binary YAML data from the file path, optionally decrypting it, and decodes it into a NestedDict.
           Parameters:
               type_dict: The type of dictionary either 'dictionary' or 'NestedDict'. If not specified, it defaults to 'NestedDict'.
           Returns:
               A NestedDict/Dict instance containing the decoded data.
        """
        with open(self.file_path, 'rb') as file:
            binary_yaml = file.read()
        if self.encryption_enabled:
            binary_yaml = self.cipher_suite.decrypt(binary_yaml)
        yaml_str = base64.b64decode(binary_yaml).decode('utf-8')
        data = yaml.safe_load(yaml_str)
        if type_dict == 'dict':
            return data
        return NestedDict(data)


    def make_backup(self, name):
        try:
            if not os.path.exists(self._backup_path):
                os.makedirs(self.backup_path)
        except FileExistsError:
            pass
        except PermissionError:
            raise PermissionError('Permission denied to create backup directory.')
        t = int(time.time())
        print(f'Backing up {self.file_path} to {self.backup_path}/{name}_{t}.ezdb')
        shutil.copy(self.file_path, f'{self._backup_path}/{name}_{t}.ezdb')
        # check if key file is backed up then back it up inside the directory.
        if not os.path.exists(f'{self.backup_path}/{name}.key') and self.encryption_enabled:
            shutil.copy(self.key_file, f'{self.backup_path}/{name}.key')
        # Return the absolute path of the backup file
        return os.path.abspath(f'{self._backup_path}/{name}_{t}.ezdb')

    def list_backups(self):
        return os.listdir(self.backup_path)

    def save_config(self):
        """
        Save the class member values to a yaml file.
        Returns:
            None
        """
        data = {
            'filepath': self.file_path,
                'encryption_enabled': self.encryption_enabled,
                'key_file': self.key_file,
                'backup_path':self.backup_path
                }
        with open('config.yaml', 'w+') as f:
            yaml.dump(data, f)

    @staticmethod
    def load_from_config(file_name='config.yaml'):
        with open(file_name, 'r') as f:
            data = yaml.safe_load(f)
        instance = BYAML(**data)
        return instance

    def export_unencrypted_yaml(self,file_name):
        data = self.decode_from_binary_yaml('dict')
        with open(file_name, 'w+') as f:
            yaml.dump(data, f)

    @staticmethod
    def convert_yaml_to_byaml(filename, encrypted=False):
        """
            Converts a YAML file to binary YAML format, optionally encrypting it.

            Parameters:
                filename (str): The path to the YAML file to convert.
                encrypted (bool, optional): Whether to encrypt the binary YAML data. Defaults to False.

            Returns:
                A tuple containing the paths to the generated binary YAML file and the key file (if encryption is enabled).
        """

        with open(filename, 'r') as f:
            x = yaml.safe_load(f)
        b = BYAML(f'{filename[:-5]}.ezdb', encrypted, f'{filename[:-5]}.key')
        b.encode_to_binary_yaml(x)
        return f'   {filename[:-5]}.ezdb', f'{filename[:-5]}.key'

    @staticmethod
    def convert_dict_to_byaml(data: dict, filename, encrypted=False):
        """Converts the dictionary to BYAML format, optionally encrypting
        Parameters:
            data (dict): The dictionary data
            filename (str): The file path as string
            encrypted (bool): Whether to encrypt the binary YAML data. Defaults to False.
        Returns:
            A string of path of the generated binary YAML file and the key file (if encrypted)
            """
        b = BYAML(f'{filename}.ezdb', encrypted, f'{filename}.key')
        b.encode_to_binary_yaml(data)
        return f'{filename}.ezdb', f'{filename}.key'
