
import logging
import threading
import time




class RecordLock:
    """
    A class that represents a record lock.
    Attributes:
        writer_lock (threading.Semaphore): A semaphore that controls access to the writer lock.
        mutex (threading.Lock): A lock that controls access to the readers count.
        readers (int): The number of active readers.
        writers (int): The number of active writers.
        writers_waiting (bool): A flag indicating if there are waiting writers.
    """

    def __init__(self):
        self.writer_lock = threading.Semaphore(1)  # Ensure only one writer at a time
        self.mutex = threading.Lock()  # Mutex for managing readers count
        self.readers = 0  # Counter for active readers
        self.writers = 0  # A count for writer.
        self.writers_waiting = False  # To denote waiting of writer


class RecordLockManager:
    """
    A class that manages record locks for a given database
    Attributes:
        records (dict): A dictionary of record locks by table name and record ID.
        mutex (threading.Lock): A lock that controls access to the records dictionary.
        logger (logging.Logger): A logger instance for logging purposes.


    """

    def __init__(self, logger=None, create_logger=False):

        self.records = {}  # Dictionary for storing locks by table name and record ID
        self.mutex = threading.Lock()  # Global mutex for synchronizing lock creation
        self.logger = logger or self._setup_default_logger(create_logger)

    def _setup_default_logger(self, create_logger):
        if create_logger:
            logger = logging.getLogger("RecordLockManager")
            handler = logging.StreamHandler()
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            handler.setFormatter(formatter)
            logger.addHandler(handler)
            logger.setLevel(logging.DEBUG)
            return logger
        return None

    def make_record_lock(self, table_name, record_id):
        """
        Creates a new record lock for the given table and record ID.
        Parameters:
            table_name (str): The name of the table.
            record_id (str): The ID of the record.
        """

        if table_name not in self.records:
            self.records[table_name] = {}
        if record_id not in self.records[table_name]:
            self.records[table_name][record_id] = RecordLock()
            self.logger.debug(f"Record lock created for {table_name} ID: {record_id}")

    def acquire_writer_lock(self, table_name, record_id):
        """
        Acquires a writer lock for the given table and record ID.
        Parameters:
            table_name (str): The name of the table.
            record_id (str): The ID of the record.
        Returns:
            None
        """
        with self.mutex:
            self.make_record_lock(table_name, record_id)
        lock = self.records[table_name][record_id]
        lock.writers_waiting = True
        self.logger.debug(f"Writer waiting to acquire lock for {table_name} record {record_id}")
        lock.writer_lock.acquire()
        lock.writers_waiting = False
        lock.writers += 1
        self.logger.debug(
            f"Writer acquired lock for {table_name} record {record_id}. Active writers: {lock.writers}")

    def release_writer_lock(self, table_name, record_id):
        """
        Releases a writer lock for the given table and record ID.

        Parameters:
            table_name (str): The name of the table.
            record_id (str): The ID of the record.

        Returns:
             None
        """
        lock = self.records[table_name][record_id]
        with lock.mutex:
            lock.writers -= 1
        lock.writer_lock.release()
        self.logger.debug(
            f"Writer released lock for {table_name} record {record_id}. Remaining writers: {lock.writers}")

    def acquire_reader_lock(self, table_name, record_id):
        """
        Acquires a reader lock for the given table and record ID.
        Parameters:
            table_name (str): The name of the table.
            record_id (str): The ID of the record.
        Returns:
            None

        """
        lock = self.records[table_name][record_id]
        with lock.mutex:
            while lock.writers_waiting:
                lock.mutex.release()
                time.sleep(0.1)
                lock.mutex.acquire()
            lock.readers += 1
            if lock.readers == 1:
                lock.writer_lock.acquire()
        self.logger.debug(f"Reader acquired lock for {table_name} record {record_id}, total readers: {lock.readers}")

    def release_reader_lock(self, table_name, record_id):
        """
        Releases a reader lock for the given table and record ID.
        Parameters:
            table_name (str): The name of the table.
            record_id (str): The ID of the record.
        Returns:
            None
        """
        lock = self.records[table_name][record_id]
        with lock.mutex:
            lock.readers -= 1
            if lock.readers == 0:
                lock.writer_lock.release()
        self.logger.debug(
            f"Reader released lock for {table_name} record {record_id}, remaining readers: {lock.readers}")

    def delete_lock_id(self, table_name, record_id):
        """
        Deletes the record lock for the given table and record ID.

        Parameters:
            table_name (str): The name of the table.
            record_id (str): The ID of the record.

        Returns:
            None
        """
        if table_name in self.records and record_id in self.records[table_name]:
            del self.records[table_name][record_id]
            self.logger.debug(f"Record lock deleted for {table_name} ID: {record_id}")

    def delete_lock_table(self, table_name):
        """
        Deletes all record locks for the given table.
        Parameters:
            table_name (str): The name of the table.
        Returns:
            None
        """
        with self.mutex:
            if table_name in self.records:
                for record_id in list(self.records[table_name].keys()):
                    self.release_reader_lock(table_name, record_id)
                    self.release_writer_lock(table_name, record_id)
                del self.records[table_name]
                self.logger.debug(f"All record locks deleted for {table_name}")
